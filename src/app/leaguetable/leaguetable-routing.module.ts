import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LeaguetableComponent} from './leaguetable.component';


const routes: Routes = [
  {path: 'league/:id/table', component: LeaguetableComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LeaguetableRoutingModule { }
