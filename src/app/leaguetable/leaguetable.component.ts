import {Component, OnInit, ViewChild} from '@angular/core';
import {League} from '../vm/League';
import {LeagueService} from '../services/league.service';
import {TeamplayerService} from '../services/teamplayer.service';
import {TeamPlayerStat} from '../vm/TeamPlayerStat';
import {Player} from '../vm/Player';
import {PlayerData} from '../vm/PlayerData';
import {ChartData} from '../vm/ChartData';
import {RankingDataset} from '../vm/RankingDataset';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './leaguetable.component.html',
  styleUrls: ['./leaguetable.component.css']
})
export class LeaguetableComponent implements OnInit {

  statsList: TeamPlayerStat[];
  statsOpponentList: TeamPlayerStat[];
  showDashboard: boolean;
  selectAll: boolean;
  public teamLabels: Array<string> = ['Gardien', 'Défense', 'Milieu', 'Attaque'];
  public teamDatasets: Array<ChartData> = [];
  public opponentTeamDatasets: Array<ChartData> = [];
  public teamRankingDatasets: Array<any> = [{ data: [], label: 'Classement général des équipes' }];
  public teamRankingChartLabels: Array<string> = [];

  public rankingHistoryLabels: Array<string> = [];
  public rankingHistoryDatasets: Array<ChartData> = [];
  public staticRankingHistoryDatasets: Array<ChartData> = [];

  public lineChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(0, 158, 251, .2)',
      borderColor: 'rgba(0, 158, 251, 1)',
      borderWidth: 2
    },
    {
      backgroundColor:'rgba(235, 59, 90, .2)',
      borderColor: 'rgba(235, 59, 90, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(247, 183, 49, .2)',
      borderColor: 'rgba(247, 183, 49, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(32, 191, 107, .2)',
      borderColor: 'rgba(32, 191, 107, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(165, 94, 234, .2)',
      borderColor: 'rgba(165, 94, 234, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(75, 101, 132, .2)',
      borderColor: 'rgba(75, 101, 132, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(250, 130, 49, .2)',
      borderColor: 'rgba(250, 130, 49, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(56, 103, 214, .2)',
      borderColor: 'rgba(56, 103, 214, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(115, 198, 182, .2)',
      borderColor: 'rgba(115, 198, 182, 1)',
      borderWidth: 2
    },
    {
      backgroundColor: 'rgba(255, 87, 51, .2)',
      borderColor: 'rgba(255, 87, 51, 1)',
      borderWidth: 2
    }
  ];
  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(0, 158, 251, .2)'
      ],
      borderColor: [
        'rgba(0, 158, 251, 1)'
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(235, 59, 90, .2)',
      ],
      borderColor: [
        'rgba(235, 59, 90, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(247, 183, 49, .2)',
      ],
      borderColor: [
        'rgba(247, 183, 49, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(32, 191, 107, .2)',
      ],
      borderColor: [
        'rgba(32, 191, 107, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(165, 94, 234, .2)',
      ],
      borderColor: [
        'rgba(165, 94, 234, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(75, 101, 132, .2)',
      ],
      borderColor: [
        'rgba(75, 101, 132, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(250, 130, 49, .2)',
      ],
      borderColor: [
        'rgba(250, 130, 49, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(56, 103, 214, .2)',
      ],
      borderColor: [
        'rgba(56, 103, 214, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(115, 198, 182, .2)',
      ],
      borderColor: [
        'rgba(115, 198, 182, 1)',
      ],
      borderWidth: 2,
    },
    {
      backgroundColor: [
        'rgba(255, 87, 51, .2)',
      ],
      borderColor: [
        'rgba(255, 87, 51, 1)',
      ],
      borderWidth: 2,
    }
  ];
  public barchartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(0, 158, 251, .2)',
        'rgba(235, 59, 90, .2)',
        'rgba(247, 183, 49, .2)',
        'rgba(32, 191, 107, .2)',
        'rgba(165, 94, 234, .2)',
        'rgba(75, 101, 132, .2)',
        'rgba(250, 130, 49, .2)',
        'rgba(56, 103, 214, .2)',
        'rgba(115, 198, 182, .2)',
        'rgba(255, 87, 51, .2)'
      ],
      borderColor: [
        'rgba(0, 158, 251, 1)',
        'rgba(235, 59, 90, 1)',
        'rgba(247, 183, 49, 1)',
        'rgba(32, 191, 107, 1)',
        'rgba(165, 94, 234, 1)',
        'rgba(75, 101, 132, 1)',
        'rgba(250, 130, 49, 1)',
        'rgba(56, 103, 214, 1)',
        'rgba(115, 198, 182, 1)',
        'rgba(255, 87, 51, 1)'
      ],
      borderWidth: 2,
    }
  ];

  public chartOptions: any = {responsive: true};

  constructor(private leagueService: LeagueService,
              private route: ActivatedRoute,
              private teamplayerService: TeamplayerService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.initComponent(params['id']);
    });

  }

  initComponent(ligueId: string) {
    this.statsList= [];
    this.statsOpponentList= [];
    let those = this;
    this.leagueService.getPlayers(ligueId).subscribe((players) => {
      players.forEach(function (player) {
        those.leagueService.getLeagueUserData(ligueId, player.userId).subscribe((leagueIndex) => {
          those.statsList.push(those.getPLayerStat(player, leagueIndex.player));
          those.statsOpponentList.push(those.getPLayerStat(player, leagueIndex.opponent));
          those.teamDatasets = those.getTeamDataSets(those.statsList);
          those.opponentTeamDatasets = those.getTeamDataSets(those.statsOpponentList);
          those.getTeamRankingDataSets(those.statsList);
        });
      });
    });

    this.leagueService.getLeagueRankingHistory(ligueId).subscribe((response) => {
      this.rankingHistoryDatasets = response.dataSets;
      this.staticRankingHistoryDatasets = response.dataSets;
      this.rankingHistoryLabels = response.chartLabels;
    });

    this.showDashboard = true;
    this.selectAll = true;

  }

  getTeamRankingDataSets(teamStatsList: TeamPlayerStat[]) {
    this.teamRankingDatasets[0].data = [];
    this.teamRankingChartLabels = [];
    for (let player of teamStatsList) {
      if (player.active) {
        this.teamRankingDatasets[0].data.push(player.averageRanking);
        this.teamRankingChartLabels.push(player.name);
      }
    }
  }

  getTeamDataSets(teamStatsList: TeamPlayerStat[]) {
    let listStat = Array<ChartData>();
    for (let player of teamStatsList) {
      if (player.active) {
        listStat.push(this.getPlayerStatData(player));
      } else {
        listStat.push(new ChartData());
      }
    }
    return listStat;
  }

  getPLayerStat(player: Player, playerData: PlayerData): TeamPlayerStat {
    let playerStat = new TeamPlayerStat();
    playerStat.name = player.name;
    playerStat.userId = player.userId;
    playerStat.averageRanking = playerData.averageRanking;
    playerStat.goalKeeperAverageRating= playerData.goalKeeperAverageRating;
    playerStat.defenseAverageRating= playerData.defenseAverageRating;
    playerStat.midfielAveragedRating= playerData.midfielAveragedRating;
    playerStat.attackAveragedRating= playerData.attackAveragedRating;
    playerStat.averageRotaldos= playerData.averageRotaldos;
    playerStat.rotaldos= playerData.rotaldos;
    playerStat.nbBadges= playerData.nbBadges;
    playerStat.goals= playerData.goals;
    playerStat.active = true;

    return playerStat;
  }


  getPlayerStatData(player: TeamPlayerStat): ChartData {
    let playerStatData = new ChartData();
    playerStatData.data.push(player.goalKeeperAverageRating);
    playerStatData.data.push(player.defenseAverageRating);
    playerStatData.data.push(player.midfielAveragedRating);
    playerStatData.data.push(player.attackAveragedRating);
    playerStatData.label =  player.name;
    return playerStatData;
  }


  showGraphs() { this.showDashboard = false; }
  showDash() {this.showDashboard = true;}

  toggleActive(playerTeam: TeamPlayerStat) {
    let newActive = !playerTeam.active;
    playerTeam.active = newActive;
    this.statsOpponentList.find(playerOpp => playerOpp.userId === playerTeam.userId)
      .active = newActive;

    this.updateCharts();
  }

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }

  updateCharts() {
    this.teamDatasets = this.getTeamDataSets(this.statsList);
    this.opponentTeamDatasets = this.getTeamDataSets(this.statsOpponentList);
    this.getTeamRankingDataSets(this.statsList);
    this.rankingHistoryDatasets = this.getRankingHistoryDataSet();

  }

  toggleSelectionAll() {
    this.selectAll = !this.selectAll;
    this.statsList.forEach(teampPlayer => teampPlayer.active = this.selectAll);
    this.statsOpponentList.forEach(teampPlayer => teampPlayer.active = this.selectAll);
    this.updateCharts();
  }

  getRankingHistoryDataSet() {
    let listStat = Array<RankingDataset>();
    for (let player of this.statsList) {
      if (player.active) {
        listStat.push(this.staticRankingHistoryDatasets.find(rankingTeamHistory => rankingTeamHistory.label === player.name));
      } else {
        listStat.push(new RankingDataset());
      }
    }
    return listStat;
  }

}
