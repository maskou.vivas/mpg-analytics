import { Component, OnInit } from '@angular/core';
import {LeagueService} from '../services/league.service';
import {ActivatedRoute} from '@angular/router';
import {LeagueIndex} from '../vm/LeagueIndex';
import {TeamplayerService} from '../services/teamplayer.service';
import {ChartData} from '../vm/ChartData';
import {Match} from '../vm/Match';

@Component({
  selector: 'app-teamstats',
  templateUrl: './teamstats.component.html',
  styleUrls: ['./teamstats.component.css']
})
export class TeamstatsComponent implements OnInit {
  myteam: string;
  userId: string;
  showDashboard: boolean;
  stats: LeagueIndex;
  matches : Match[];

  public teamLabels: Array<string> = ['Gardien', 'Défense', 'Milieu', 'Attaque'];
  public playerOpponentLabels: Array<string> = [];

  public teamDatasets: Array<ChartData> = [];
  public goalsDatasets: Array<ChartData> = [];
  public rankingDatasets: Array<ChartData> = [];
  public rotaldosDatasets: Array<ChartData> = [];

  public barChartColors: Array<any> = [
    {
      backgroundColor: 'rgba(0, 158, 251 , .2)',
      borderColor: 'rgba(0, 158, 251 , .7)',
      borderWidth: 2,
    },
    {
      backgroundColor: 'rgba(235, 59, 90, .2)',
      borderColor: 'rgba(235, 59, 90, .7)',
      borderWidth: 2,
    }
  ];
  public chartColors: Array<any> = [
    {
      backgroundColor: [
        'rgba(0, 158, 251 , .2)',
        'rgba(235, 59, 90, .2)'
      ],
      borderColor: [
        'rgba(0, 158, 251 , 1)',
        'rgba(235, 59, 90 , 1)'
      ],
      borderWidth: 2,
    }
  ];
  public chartOptions: any = {responsive: true};

  constructor(private leagueService: LeagueService, private route: ActivatedRoute,
              private teamplayerService: TeamplayerService) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.initComponent(params['leagueid'], params['userid']); // reset and set based on new parameter this time
    });
  }

  initComponent(leagueId: string, userId: string) {
    this.stats = undefined;
    this.userId = userId;
    this.leagueService.getUserLastGames(leagueId, userId).subscribe((matches) => {
      this.matches = matches;
    });

    this.leagueService.getLeagueUserData(leagueId, userId).subscribe((leagueIndex) => {
      this.stats = leagueIndex;
      this.myteam = leagueIndex.teamName;

      let playerStatData = new ChartData();
      playerStatData.data.push(this.stats.player.goalKeeperAverageRating);
      playerStatData.data.push(this.stats.player.defenseAverageRating);
      playerStatData.data.push(this.stats.player.midfielAveragedRating);
      playerStatData.data.push(this.stats.player.attackAveragedRating);
      playerStatData.label =  leagueIndex.teamName;
      let opponentStatData = new ChartData();
      opponentStatData.data.push(this.stats.opponent.goalKeeperAverageRating);
      opponentStatData.data.push(this.stats.opponent.defenseAverageRating);
      opponentStatData.data.push(this.stats.opponent.midfielAveragedRating);
      opponentStatData.data.push(this.stats.opponent.attackAveragedRating);
      opponentStatData.label =  "Adversaire";
      this.teamDatasets.push(opponentStatData);
      this.teamDatasets.push(playerStatData);

      let goalStatData = new ChartData();
      goalStatData.data.push(this.stats.player.goals);
      goalStatData.data.push(this.stats.opponent.goals);
      goalStatData.label = "Buts marqués";
      this.goalsDatasets.push(goalStatData);

      let rankingStatData = new ChartData();
      rankingStatData.data.push(this.stats.player.averageRanking);
      rankingStatData.data.push(this.stats.opponent.averageRanking);
      rankingStatData.label = "Classement général moyen";
      this.rankingDatasets.push(rankingStatData);

      let rotaldosStatData = new ChartData();
      rotaldosStatData.data.push(this.stats.player.rotaldos);
      rotaldosStatData.data.push(this.stats.opponent.rotaldos);
      rotaldosStatData.label = "Nombre de rotaldos";
      this.rotaldosDatasets.push(rotaldosStatData);

    });


    this.playerOpponentLabels = [this.myteam, 'Adversaire'];
    this.showDashboard = true;
  }
  showGraphs() { this.showDashboard = false; }
  showDash() {this.showDashboard = true;}

  public chartClicked(e: any): void { }
  public chartHovered(e: any): void { }
}
