import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamstatsComponent } from './teamstats.component';
import {SharedModule} from '../shared/shared.module';
import {TeamstatsRoutingModule} from './teamstats-routing.module';
import {ChartsModule} from 'angular-bootstrap-md';



@NgModule({
  declarations: [TeamstatsComponent],
    imports: [
        CommonModule,
        TeamstatsRoutingModule,
        SharedModule,
        ChartsModule
    ]
})
export class TeamstatsModule { }
