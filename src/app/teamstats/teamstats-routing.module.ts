import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {TeamstatsComponent} from './teamstats.component';


const routes: Routes = [
  {path: 'league/:leagueid/user/:userid', component: TeamstatsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TeamstatsRoutingModule { }
