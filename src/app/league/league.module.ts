import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeagueComponent } from './league.component';
import {LeagueRoutingModule} from './league-routing.module';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [LeagueComponent],
  imports: [
    CommonModule,
    LeagueRoutingModule,
    SharedModule
  ]
})
export class LeagueModule { }
