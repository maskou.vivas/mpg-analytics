import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {LeagueService} from '../services/league.service';
import {LeagueIndex} from '../vm/LeagueIndex';
import {League} from '../vm/League';
import {Player} from '../vm/Player';
import {resetFakeAsyncZone} from '@angular/core/testing';
import {TeamplayerService} from '../services/teamplayer.service';
import {parseLazyRoute} from '@angular/compiler/src/aot/lazy_routes';
import {AuthenticationService} from '../services/authentication.service';
import {Match} from '../vm/Match';
import {User} from '../vm/User';

@Component({
  selector: 'app-league',
  templateUrl: './league.component.html',
  styleUrls: ['./league.component.css']
})
export class LeagueComponent implements OnInit {
  stats: LeagueIndex;
  matches : Match[];
  userId: string;

  constructor(private router: Router, private route: ActivatedRoute,
              private leagueService: LeagueService,
              private teamplayerService: TeamplayerService,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
    let leagueId = this.route.snapshot.paramMap.get('id');
    this.leagueService.getLeagueData(leagueId).subscribe((leagueIndex) => {
      this.stats = leagueIndex;
    });

    this.userId = this.authenticationService.user.userId;
    this.leagueService.getUserLastGames(leagueId, this.userId).subscribe((matches) => {
      this.matches = matches;
    });
  }

}
