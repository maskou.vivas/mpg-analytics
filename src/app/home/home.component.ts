import { Component, OnInit } from '@angular/core';
import {LeagueService} from '../services/league.service';
import {League} from '../vm/League';
import {Router} from '@angular/router';
import {AuthenticationService} from '../services/authentication.service';
import {LeagueWithPlayers} from '../vm/LeagueWithPlayers';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  leagues: LeagueWithPlayers[];

  constructor(private router: Router, private leagueService: LeagueService,
              private authenticationService : AuthenticationService) { }

  ngOnInit() {
    this.leagueService.getAllLeaguesWithPlayers().subscribe((leagueLst) => {
      this.leagues = leagueLst;
      this.authenticationService.leagues = leagueLst;
    });
  }

  goLeague(league) {
    this.leagueService.league = league;
    this.router.navigate(['league/' + league.id]);
  }

}
