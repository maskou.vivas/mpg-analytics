import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {LoginModule} from './login/login.module';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {HomeModule} from './home/home.module';
import {SharedModule} from './shared/shared.module';
import {LeagueModule} from './league/league.module';
import {TeamstatsModule} from './teamstats/teamstats.module';
import {LeaguetableModule} from './leaguetable/leaguetable.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MDBBootstrapModule} from 'angular-bootstrap-md';
import { ChartsModule, WavesModule } from 'angular-bootstrap-md'
import {LogoutModule} from './logout/logout.module';


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    BrowserAnimationsModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    ChartsModule,
    WavesModule,
    SharedModule,
    LoginModule,
    LogoutModule,
    LeagueModule,
    TeamstatsModule,
    LeaguetableModule,
    HomeModule,
    AppRoutingModule
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
