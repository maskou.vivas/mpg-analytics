import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from '../services/authentication.service';
import {Credentials} from '../vm/Credentials';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  user: Credentials;

  constructor(private router: Router, private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.user = {
        login: "",
        password: ""
      };
  }

  doLogin() {
    this.authenticationService.sendLogin(this.user).subscribe((user) => {
      this.authenticationService.user = user;
      this.router.navigate(['home'])
    });
  }
}
