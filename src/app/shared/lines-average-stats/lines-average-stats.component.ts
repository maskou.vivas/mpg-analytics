import {Component, Input, OnInit} from '@angular/core';
import {LeagueIndex} from '../../vm/LeagueIndex';
import {PlayerData} from '../../vm/PlayerData';
import {Bonus} from '../../vm/Bonus';

@Component({
  selector: 'app-lines-average-stats',
  templateUrl: './lines-average-stats.component.html',
  styleUrls: ['./lines-average-stats.component.css']
})
export class LinesAverageStatsComponent implements OnInit {
  @Input() teamPlayerData: PlayerData;

  constructor() { }

  ngOnInit() {
  }

}
