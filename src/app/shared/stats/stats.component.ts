import {Component, Input, OnInit} from '@angular/core';
import {LeagueIndex} from '../../vm/LeagueIndex';
import {Match} from '../../vm/Match';

@Component({
  selector: 'app-stats',
  templateUrl: './stats.component.html',
  styleUrls: ['./stats.component.css']
})
export class StatsComponent implements OnInit {
  @Input() statistiques: LeagueIndex;
  @Input() myteam: string;
  @Input() matches: Match[];
  @Input() userId: string;

  constructor() { }

  ngOnInit() {
  }

}
