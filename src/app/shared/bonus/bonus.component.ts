import {Component, Input, OnInit} from '@angular/core';
import {LeagueIndex} from '../../vm/LeagueIndex';
import {PlayerData} from '../../vm/PlayerData';
import {Bonus} from '../../vm/Bonus';

@Component({
  selector: 'app-bonus',
  templateUrl: './bonus.component.html',
  styleUrls: ['./bonus.component.css']
})
export class BonusComponent implements OnInit {
  @Input() bonus: Bonus;;

  constructor() { }

  ngOnInit() {
  }

}
