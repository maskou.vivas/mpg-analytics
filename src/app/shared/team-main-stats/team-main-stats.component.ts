import {Component, Input, OnInit} from '@angular/core';
import {LeagueIndex} from '../../vm/LeagueIndex';
import {PlayerData} from '../../vm/PlayerData';
import {Bonus} from '../../vm/Bonus';

@Component({
  selector: 'app-team-main-stats',
  templateUrl: './team-main-stats.component.html',
  styleUrls: ['./team-main-stats.component.css']
})
export class TeamMainStatsComponent implements OnInit {
  @Input() teamPlayerData: PlayerData;

  constructor() { }

  ngOnInit() {
  }

}
