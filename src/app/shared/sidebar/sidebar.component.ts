import {Component, Input, OnInit} from '@angular/core';
import {User} from '../../vm/User';
import {League} from '../../vm/League';
import {AuthenticationService} from '../../services/authentication.service';
import {LeagueService} from '../../services/league.service';
import {LeagueWithPlayers} from '../../vm/LeagueWithPlayers';
import {UserInfos} from '../../vm/UserInfos';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./side.component.css']
})
export class SidebarComponent implements OnInit {
  leaguesWithPlayers: LeagueWithPlayers[];
  connectedUser: UserInfos = new UserInfos();

  constructor(private authenticationService: AuthenticationService,
              private leagueService: LeagueService) { }

  ngOnInit() {
     this.leagueService.getAllLeaguesWithPlayers().subscribe(leagueLst => {
       this.leaguesWithPlayers = leagueLst;
    });

    this.authenticationService.getUserInfos().subscribe(infos => {
      this.connectedUser = infos;
    });
  }

}
