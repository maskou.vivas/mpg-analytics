import {Component, Input, OnInit} from '@angular/core';
import {LeagueIndex} from '../../vm/LeagueIndex';
import {PlayerData} from '../../vm/PlayerData';
import {Bonus} from '../../vm/Bonus';
import {Match} from '../../vm/Match';
import {LeagueService} from '../../services/league.service';
import {AuthenticationService} from '../../services/authentication.service';
import {User} from '../../vm/User';

@Component({
  selector: 'app-last-games',
  templateUrl: './last-games.component.html',
  styleUrls: ['./last-games.component.css']
})
export class LastGamesComponent implements OnInit {
  @Input() matches: Match[] = [];
  @Input() userStatsId : string;
  constructor(private authenticationService: AuthenticationService) { }

  ngOnInit() {
  }

}
