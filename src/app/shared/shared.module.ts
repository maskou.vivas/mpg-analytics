import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule} from "@angular/forms";
import {TopbarComponent} from './topbar/topbar.component';
import {FooterbarComponent} from './footerbar/footerbar.component';
import {RouterModule} from '@angular/router';
import { StatsComponent } from './stats/stats.component';
import {ScorersComponent} from './scorers/scorers.component';
import {TeamStatsComponent} from './team-stats/team-stats.component';
import {BonusComponent} from './bonus/bonus.component';
import {LastGamesComponent} from './last-games/last-games.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {TeamMainStatsComponent} from './team-main-stats/team-main-stats.component';
import {LinesAverageStatsComponent} from './lines-average-stats/lines-average-stats.component';

@NgModule({
    imports: [
        CommonModule,
        RouterModule
    ],
    declarations: [TopbarComponent,
      FooterbarComponent,
      StatsComponent,
      ScorersComponent,
      TeamStatsComponent,
      BonusComponent,
      LastGamesComponent,
      TeamMainStatsComponent,
      LinesAverageStatsComponent,
      SidebarComponent],
    exports: [
      CommonModule,
      TopbarComponent,
      ScorersComponent,
      BonusComponent,
      TeamStatsComponent,
      FooterbarComponent,
      LastGamesComponent,
      SidebarComponent,
      TeamMainStatsComponent,
      LinesAverageStatsComponent,
      FormsModule,
      StatsComponent
    ]
})
export class SharedModule {
}
