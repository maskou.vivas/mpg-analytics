import {Component, Input, OnInit} from '@angular/core';
import {LeagueIndex} from '../../vm/LeagueIndex';
import {Scorer} from '../../vm/Scorer';

@Component({
  selector: 'app-scorers',
  templateUrl: './scorers.component.html',
  styleUrls: ['./scorers.component.css']
})
export class ScorersComponent implements OnInit {
  @Input() scorers: Scorer[];

  constructor() { }

  ngOnInit() {
  }

}
