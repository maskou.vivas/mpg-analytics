import {Injectable} from '@angular/core';
import {Player} from '../vm/Player';


@Injectable({
  providedIn: 'root'
})
export class TeamplayerService {
  private _player: Player;
  private _players: Player[];

  constructor() {
  }

  get player(): Player {
    return this._player;
  }

  set player(value: Player) {
    this._player = value;
  }

  get players(): Player[] {
    return this._players;
  }

  set players(value: Player[]) {
    this._players = value;
  }
}
