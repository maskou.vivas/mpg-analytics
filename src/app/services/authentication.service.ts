import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {User} from '../vm/User';
import {Credentials} from '../vm/Credentials';
import {LeagueWithPlayers} from '../vm/LeagueWithPlayers';
import {UserInfos} from '../vm/UserInfos';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin':'*',
    'Accept':'*/*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private _user: User;
  private _leagues: LeagueWithPlayers[];

  constructor(private httpClient: HttpClient) {
  }

  sendLogin(user: Credentials): Observable<User> {
    return this.httpClient.post<User>("http://localhost:8080/connect", user, httpOptions);
  }

  getUserInfos(): Observable<UserInfos> {
    return this.httpClient.post<UserInfos>("http://localhost:8080/user/infos", this.user, httpOptions);
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }

  get leagues(): LeagueWithPlayers[] {
    return this._leagues;
  }

  set leagues(value: LeagueWithPlayers[]) {
    this._leagues = value;
  }
}
