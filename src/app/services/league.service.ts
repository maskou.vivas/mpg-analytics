import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs/index';
import {User} from '../vm/User';
import {Credentials} from '../vm/Credentials';
import {League} from '../vm/League';
import {AuthenticationService} from './authentication.service';
import {LeagueIndex} from '../vm/LeagueIndex';
import {Player} from '../vm/Player';
import {RankingHistory} from '../vm/RankingHistory';
import {LeagueWithPlayers} from '../vm/LeagueWithPlayers';
import {Match} from '../vm/Match';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Access-Control-Allow-Origin':'*',
    'Accept':'*/*'
  })
};

@Injectable({
  providedIn: 'root'
})
export class LeagueService {
  private _league: League;

  constructor(private httpClient: HttpClient,
              private authenticationService: AuthenticationService) {
  }

  getAllLeaguesWithPlayers(): Observable<LeagueWithPlayers[]> {
    let user = this.authenticationService.user;
    return this.httpClient.post<LeagueWithPlayers[]>("http://localhost:8080/league/all", user);
  }


  getLeagueData(leagueId: string): Observable<LeagueIndex> {
    let user = this.authenticationService.user;
    return this.httpClient.post<LeagueIndex>("http://localhost:8080/league/" + leagueId, user);
  }

  getLeagueUserData(leagueId: string, userId: string): Observable<LeagueIndex> {
    let user = this.authenticationService.user;
    return this.httpClient.post<LeagueIndex>("http://localhost:8080/league/" + leagueId + "/user/" + userId, user);
  }

  getUserLastGames(leagueId: string, userId: string): Observable<Match[]> {
    let user = this.authenticationService.user;
    return this.httpClient.post<Match[]>("http://localhost:8080/league/" + leagueId + "/user/" + userId + "/games", user);

  }

  getPlayers(leagueId: string): Observable<Player[]> {
    let user = this.authenticationService.user;
    return this.httpClient.post<Player[]>("http://localhost:8080/players/" + leagueId , user);

  }

  getLeagueRankingHistory(leagueId: string): Observable<RankingHistory> {
    let user = this.authenticationService.user;
    return this.httpClient.post<RankingHistory>("http://localhost:8080/league/" + leagueId + "/ranking/history", user);
  }

  get league(): League {
    return this._league;
  }

  set league(value: League) {
    this._league = value;
  }

}
