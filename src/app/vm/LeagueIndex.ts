import {PlayerData} from './PlayerData';

export class LeagueIndex {
  name: string;
  teamName: string;
  player: PlayerData;
  opponent: PlayerData;
}
