export class League {
  id: string;
  admin_mpg_user_id: string;
  current_mpg_user: string;
  name: string;
  url: string;
  leagueStatus: number;
  championship: number;
  mode: number;
  teamStatus: number;
  players: number;
}
