
export class TeamPlayerStat {
  name: string;
  userId: string;
  averageRanking: number;
  goalKeeperAverageRating: number;
  defenseAverageRating: number;
  midfielAveragedRating: number;
  attackAveragedRating: number;
  averageRotaldos: number;
  rotaldos: number;
  nbBadges: number;
  goals: number;
  active: boolean;

}
