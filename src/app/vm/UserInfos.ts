export class UserInfos {
  id: string;
  email: string;
  firstname: string;
  lastname: string;
  country: string;
  teamId: number;
  dob: string;
  onboarded: boolean;
  push_wall: number;
  optin_nl: number;
  optin_partner: number;
  jerseySkin: string[];
  money: number;
  bounce: number;
  hasBeenExpert: boolean;
  optinNlMpp: number;
  pushNlMpp: number;
  liveRating: string;
}
