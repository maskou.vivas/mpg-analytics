import {TeamResult} from './TeamResult';

export class Match {
  id : string;
  teamAway: TeamResult;
  teamHome: TeamResult;
}
