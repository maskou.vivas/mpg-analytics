import {Player} from './Player';
import {League} from './League';

export class LeagueWithPlayers {
  leagueInfo: League;
  players: Player[];

}
