import {Bonus} from './Bonus';
import {Scorer} from './Scorer';

export class PlayerData {
   averageRanking: number;
   goalKeeperAverageRating: number;
   defenseAverageRating: number;
   midfielAveragedRating: number;
   attackAveragedRating: number;
   teamAverageRating: number;
   rotaldos: number;
   averageRotaldos: number;
   nbBadges: number;
   goals: number;
   bonus: Bonus;
   scorers : Scorer[];
}
