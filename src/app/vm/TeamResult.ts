export class TeamResult {
  id: string;
  level: string;
  name: string;
  score: string;
  targetMan: string;
  user: string;
}
