import {RankingDataset} from './RankingDataset';

export class RankingHistory {
  dataSets: RankingDataset[];
  chartLabels: string[];

}
